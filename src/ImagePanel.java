
import Geometric.MyDrawable;

import java.awt.*;
import javax.swing.*;
import java.awt.image.VolatileImage;
import java.util.ArrayList;

public class ImagePanel extends JPanel {

    /**
     * The background color that is used to fill the off-screen canvas when
     * it is created.  If the user selects the "Fill With Color", the fill
     * color changes, and the canvas is filled with the new fill color,
     * erasing whatever was there before.
     */
    private Color fillColor = Color.WHITE;

    /**
     * The off-screen canvas.  This is not created until the first time
     * paintComponent() is called.  If the size of the component changes,
     * a new image is created (and the picture in the old one is lost).
     */
    private VolatileImage mainPanel;


    private final int width;
    private final int height;

    /**
     * This contain all the images loaded to display
     */
    private ArrayList<Images> images = new ArrayList<>();

    /**
     * The constructor sets the preferred size of the panel
     */
    public ImagePanel(int width, int height)
    {
        this.width  = width;
        this.height = height;
        setPreferredSize(new Dimension(width, height));
    }

    /**
     * The constructor sets the preferred size of the panel
     * And init the images array
     */
    public ImagePanel(int width, int height, ArrayList<Images> images)
    {
        this.width  = width;
        this.height = height;
        setPreferredSize(new Dimension(width, height));
        this.images = images;
    }

    public void setImages(ArrayList<Images> images) {
        this.images = images;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (mainPanel == null) // création de l'mainPanel
        {
            mainPanel = createVolatileImage(getWidth(), getHeight());
            Graphics2D gg = mainPanel.createGraphics();
            gg.setColor(fillColor);
            gg.fillRect(0, 0, getWidth(), getHeight());
        }
        do {
            int code = mainPanel.validate(getGraphicsConfiguration());
            switch(code) {
                case VolatileImage.IMAGE_INCOMPATIBLE:
                    mainPanel = createVolatileImage(getWidth(), getHeight());
                case VolatileImage.IMAGE_RESTORED:
                    reCreer(); // redessiner l'mainPanel
            }
            if (mainPanel.validate(getGraphicsConfiguration())==VolatileImage.IMAGE_OK)
                g.drawImage(mainPanel, 0, 0, this);

        } while (mainPanel.contentsLost());
    }

    void reCreer()
    {
        Graphics2D g = mainPanel.createGraphics();
        for (Images img: images)
            g.drawImage(img.createImageWithShapes(), (int)img.getPosition().getX(), (int)img.getPosition().getY(),null);
    }


    @Override
    public int getWidth()
    {
        return width;
    }

    @Override
    public int getHeight()
    {
        return height;
    }

    public VolatileImage getImage() {
        return mainPanel;
    }

    public void setImage(VolatileImage mainPanel) {
        this.mainPanel = mainPanel;
    }

     public static VolatileImage createVolatileImage(int width, int height, int transparency) {
        if (width == 0 || height == 0)
            return null;

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsConfiguration gc = ge.getDefaultScreenDevice().getDefaultConfiguration();
        VolatileImage image = null;

        image = gc.createCompatibleVolatileImage(width, height, transparency);

        int valid = image.validate(gc);

        if (valid == VolatileImage.IMAGE_INCOMPATIBLE)
            image = createVolatileImage(width, height, transparency);

        return image;
    }
}
