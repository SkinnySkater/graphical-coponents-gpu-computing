import Geometric.MyDrawable;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.VolatileImage;
import java.util.ArrayList;

/**
 * Created by root on 4/10/17.
 */
public class Images
{
    /** PARAMETERS **/
    private Point2D position;
    private VolatileImage image;
    private int width, height;
    private ArrayList<MyDrawable> shapes = new ArrayList<MyDrawable>();
    /** transformation parameters **/
    private float scalex1, scalex2 = 1;
    private float rot = 0;
    private float trans1, trans2 = 0;

    /** CONSTRUCTOR **/
    public Images(Point2D position, int width, int height, ArrayList<MyDrawable> org_shapes)
    {
        this.position = position;
        this.width = width;
        this.height = height;
        this.image = ImagePanel.createVolatileImage(width, height, Transparency.OPAQUE);
        this.shapes = org_shapes;
    }

    /** CONSTRUCTOR **/
    public Images(Point2D position, int width, int height)
    {
        this.position = position;
        this.width = width;
        this.height = height;
        this.image = ImagePanel.createVolatileImage(width, height, Transparency.OPAQUE);
    }

    /**
     * Create an image based on the given shape
     * through the
     */
    public Image createImageWithShapes(){
      Graphics g = image.getGraphics();
        for (MyDrawable shape: shapes)
            shape.draw((Graphics2D) g);

      return image;
   }

    /** SETTER **/
    public void setScalex1(float scalex1) {
        this.scalex1 = scalex1;
    }

    public void setScalex2(float scalex2) {
        this.scalex2 = scalex2;
    }

    public void setRot(float rot) {
        this.rot = rot;
    }

    public void setTrans1(float trans1) {
        this.trans1 = trans1;
    }

    public void setTrans2(float trans2) {
        this.trans2 = trans2;
    }

    public void setShapes(ArrayList<MyDrawable> shapes) {
        this.shapes = shapes;
    }

    public VolatileImage getImage() {
        return image;
    }

    public ArrayList<MyDrawable> getShapes() {
        return shapes;
    }

    public double getAreaSum()
    {
        float area = 0;
        for (MyDrawable shape: shapes)
            area += shape.ComputeArea();
        return area;
    }

    public double getPerimSum()
    {
        float perim = 0;
        for (MyDrawable shape: shapes)
            perim += shape.ComputePerim();
        return perim;
    }

    public Point2D getPosition() {
        return position;
    }
}
