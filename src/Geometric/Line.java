package Geometric;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * Created by root on 4/9/17.
 */
public abstract class Line implements MyDrawable
{
    /** ATTRIBUTES **/
    private Line2D.Double line;
    private Point2D p1, p2;
    private float scalex1, scalex2 = 1;
    private float rot = 0;
    private float trans1, trans2 = 0;
    private final Shape type = Shape.LINE;

    /** CONSTRUCTOR **/
    public Line(Point2D p1, Point2D p2)
    {
        this.p1 = p1;
        this.p2 = p2;
    }

    public void setScalex1(float scalex1) {
        this.scalex1 = scalex1;
    }

    public void setScalex2(float scalex2) {
        this.scalex2 = scalex2;
    }


    /** MY DRAWABLE METHODS **/

    /**
     * Draw the given line
     * @param graphics: graphics to draw
     */
    public void draw(Graphics2D graphics)
    {
        AffineTransform transform = new AffineTransform();
        line = new Line2D.Double(p1, p2);
        graphics.setColor(drawColor);
        /** OPERATE TRANSLATION **/
        transform.translate(this.trans1, this.trans2);
        /** OPERATE SCALE **/
        transform.scale(this.scalex1, this.scalex2);
        /** OPERATE ROTATION **/
        transform.rotate(this.rot);
        /** PERFORM ALL THE TRANSFORMATIONS **/
        graphics.setTransform(transform);
        /**  THEN DRAW THE LINE **/
        graphics.draw(line);
    }


     /**
     * Update rotation param of the line
     * @param new_rot: rotation angle to add
     */
    public void doRot(float new_rot)
    {
        this.rot += new_rot;
    }

    /**
     * Update scale params of the line
     * @param new_sc1: scale X axis angle to change
     * @param new_sc2: scale Y-axis angle to change
     */
    public void doScale(float new_sc1, float new_sc2)
    {
        this.scalex1 = new_sc1;
        this.scalex2 = new_sc2;
    }

    public void doTrans(float new_tr1, float new_tr2)
    {
        this.trans1 += new_tr1;
        this.trans2 += new_tr2;
    }


    /** SHAPES METHODS **/

    /**
     * Return the Area amount
     */
    public double ComputeArea()
    {
        return Math.max(Math.abs(p2.getX() - p1.getX()), Math.abs(p2.getY() - p1.getY()));
    }

    /**
     * Return the Perimetre
     */
    public double ComputePerim()
    {
        return Math.max(Math.abs(p2.getX() - p1.getX()), Math.abs(p2.getY() - p1.getY()));
    }
}
