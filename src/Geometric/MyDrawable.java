package Geometric;

import java.awt.*;
import java.awt.image.VolatileImage;

/**
 * Created by root on 4/9/17.
 */
public interface MyDrawable
{
    enum Shape { LINE, RECT, ELLIPSE, POLY }
    Color eraseColor = Color.WHITE;
    Color drawColor = Color.BLACK;

    /** MAIN FUNCTIONS OF A DRAWABLE **/
    public void draw(Graphics2D g);

    /** ALL THE TRANSFORMATIONS A DRAWABLE OBJ CAN GO THROUGH **/
    public void doRot(float new_rot);
    public void doScale(float new_sc1, float new_sc2);
    public void doTrans(float new_tr1, float new_tr2);

    /**TODO : ADD THE CENTER SYMETRIC & AXIAL METHODS **/

     public double ComputeArea();
     public double ComputePerim();
}
