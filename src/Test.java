import Geometric.Line;
import Geometric.MyDrawable;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by root on 4/9/17.
 */
public class Test
{
    static Random rand = new Random();
    static int width  = 1200;
    static int height =  800;
    /**
     * The main routine simply opens a window that shows an Image panel.
     * and Initialize all the drawable components.
     */
    public static void main(String[] args)
    {
        JFrame window = new JFrame("PaintWithOffScreenCanvas");
        ImagePanel  content = new ImagePanel(width, height);
        window.setContentPane(content);
        window.pack();
        window.setResizable(true);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        window.setLocation((screenSize.width - window.getWidth()) / 2,
                (screenSize.height - window.getHeight()) / 2);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);

        System.out.println("INITIALIZATION OF GRAPHICAL COMPONENTS");
        /** Create objects **/
        int imgNmb = rand.nextInt(10);   /** Number of image contained in The drawing **/
        int shapeNmb = rand.nextInt(4);  /** Number of geometric shapes in one image **/
        System.out.println("The process gonna use ");
        Images img;
        ArrayList<MyDrawable> shapes = new ArrayList<>();
        ArrayList<Images> imagesArrayList = new ArrayList<>();
        Line line;
        Point2D p1, p2;
        for (int i = 0; i < imgNmb; i++)
        {
            /** Set up alll the parameters for the current image. **/
            p1 = new Point2D.Double(rand.nextInt(width - 100), rand.nextInt(height - 100));
            img = new Images(p1, width, height);
            for (int j = 0; j < shapeNmb; j++)
            {
                /** GET THE SHAPE NUMBER TO CREATE THE MATCHING SHAPE **/
                switch (j)
                {
                    case 0:
                        p1 = new Point2D.Double(rand.nextInt(width), rand.nextInt(height));
                        p2 = new Point2D.Double(rand.nextInt(width), rand.nextInt(height));
                        line = new Line(p1, p2) {};
                        shapes.add(line);
                        break;
                    default:
                        p1 = new Point2D.Double(rand.nextInt(width), rand.nextInt(height));
                        p2 = new Point2D.Double(rand.nextInt(width), rand.nextInt(height));
                        line = new Line(p1, p2) {};
                        shapes.add(line);
                        break;
                }
            }
            /** Set up all the shapes in the list then add them to the current image**/
            img.setShapes(shapes);
            imagesArrayList.add(img);
        }
        content.setImages(imagesArrayList);
    }
}
